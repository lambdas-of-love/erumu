window.onkeydown = function(e) {
    if (e.keyCode == 8 && e.target == document.body)
      e.preventDefault();
  }

import './main.css';
import { Main } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

Main.embed(document.getElementById('root'));

registerServiceWorker();

window.onkeydown = function(e) {
    if (e.keyCode == 8 && e.target == document.body)
      e.preventDefault();
  }
