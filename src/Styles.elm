--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Styles exposing (..)

import Html exposing (Html)
import Html.Attributes exposing (..)
import Animate.Css


redColor =
    "#da635d"


tanColor =
    "#f7e1d3"


salmonColor =
    "#f5d2d3"


pinkColor =
    "#dfccf1"


greenColor =
    "#bdd0c4"


brownColor =
    "#b1938b"


blueColor =
    "#9ab7d3"


blackColor =
    "#4e4e56"


cardColor =
    tanColor


roundedBoxStyle : Html.Attribute msg
roundedBoxStyle =
    style
        [ ( "border-radius", "1em" )
        , ( "display", "flex" )
        , ( "align-items", "center" )
        , ( "color", blackColor )
        , ( "flex-direction", "column" )
        , ( "padding", "1em" )
        , ( "width", "15em" )
        , ( "background", cardColor )
        , ( "box-shadow", "0 0 40px rgba(0,0,0,.3)" )
        ]


backgroundStyle : Html.Attribute msg
backgroundStyle =
    style
        [ ( "display", "flex" )
        , ( "padding", "3em" )
        , ( "align-items", "center" )
        , ( "justify-content", "center" )
        ]


textInputStyle : Html.Attribute msg
textInputStyle =
    style
        [ ( "font-size", "2em" )
        , ( "background", "transparent" )
        , ( "color", blackColor )
        , ( "text-align", "center" )
        ]


hintStyle : Html.Attribute msg
hintStyle =
    style
        [ ( "display", "flex" )
        ]


hintTextStyle : Html.Attribute msg
hintTextStyle =
    style
        [ ( "font-size", ".5em" )
        , ( "padding-left", ".5em" )
        , ( "padding-right", ".5em" )
        , ( "color", redColor )
        ]


noHintStyle : Html.Attribute msg
noHintStyle =
    style
        [ ( "display", "flex" )
        , ( "color", cardColor )
        ]


autoCompleteStyle : Html.Attribute msg
autoCompleteStyle =
    style
        [ ( "display", "flex" )
        , ( "flex-direction", "row" )
        , ( "flex", "1 0 4" )
        , ( "padding-bottom", ".5em" )
        ]


autoCompleteTextStyle : Html.Attribute msg
autoCompleteTextStyle =
    style
        [ ( "font-size", ".5em" )
        , ( "padding-left", ".5em" )
        , ( "padding-right", ".5em" )
        ]


progressStyle : Html.Attribute msg
progressStyle =
    style
        [ ( "font-size", ".5em" )
        , ( "padding-top", ".5em" )
        , ( "color", greenColor )
        ]


bigTextStyle : Html.Attribute msg
bigTextStyle =
    style
        [ ( "font-size", "10em" )
        , ( "color", blueColor )
        ]
