--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module View exposing (view)

import Html exposing (Html, text, div, img, input)
import Html.Attributes exposing (..)
import Animate.Css
import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Row as Row
import Bootstrap.Grid.Col as Col
import Model exposing (..)
import Styles exposing (..)
import Util exposing (..)
import Answers exposing (..)


autoComplete : Model -> Html Msg
autoComplete model =
    div [ autoCompleteStyle ]
        (List.map
            (\pair ->
                pair
                    |> text
                    |> List.singleton
                    |> div [ autoCompleteTextStyle ]
            )
            (getMatches model.content
                hiraganaMonoGraphs
            )
        )


hints : List String -> Html msg
hints modelHints =
    if (List.isEmpty modelHints) then
        div [ noHintStyle ]
            [ div [ autoCompleteTextStyle ] [ text "|" ]
            ]
    else
        div [ hintStyle ]
            (div [ hintTextStyle ] [ text "hints:" ]
                :: (List.map
                        (\hint -> div [ autoCompleteTextStyle ] [ text hint ])
                        modelHints
                   )
            )


view : Model -> Html Msg
view model =
    div [ backgroundStyle ]
        [ div [ roundedBoxStyle ]
            [ div [] [ text "hiragana" ]
            , div [ progressStyle ] [ String.concat [ toString model.won, "/", toString model.guesses ] |> text ]
            , div
                (if model.playIncorrectAnimation then
                    [ bigTextStyle, class Animate.Css.animated, class Animate.Css.shake ]
                 else
                    [ bigTextStyle ]
                )
                [ case model.question of
                    Nothing ->
                        text " "

                    Just question ->
                        text question
                ]
            , div [] [ autoComplete model ]
            , hints model.currentHints
            , (if String.isEmpty model.content then
                div [ textInputStyle, noHintStyle ] [ text "|" ]
               else
                div [ textInputStyle ] [ text model.content ]
              )
            ]
        ]
