--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Util exposing (..)

import Random
import Random.List
import Set
import Answers exposing (..)


calculateHints : AnswerBank -> Random.Seed -> Maybe String -> ( List String, Random.Seed )
calculateHints answerBank seed answer =
    case answer of
        Nothing ->
            ( [], seed )

        Just kana ->
            let
                -- answer is a kana, we need to find the actual expected answer first
                expectedAnswer =
                    answerBank
                        |> List.filter (\( a, b ) -> a == kana)
                        |> List.map Tuple.second

                -- create a list where the answer isn't in it.
                redHerrings =
                    answerBank
                        |> List.filter (\( a, b ) -> a /= kana)
                        |> List.map Tuple.second

                runner =
                    redHerrings
                        |> Random.List.shuffle
                        |> Random.step

                ( shuffled, newSeed ) =
                    runner seed
            in
                ( List.concat [ List.take 2 shuffled, expectedAnswer ]
                    |> List.sort
                , newSeed
                )


getMatches : String -> AnswerBank -> List String
getMatches input answerBank =
    let
        bank =
            answerBank
                |> List.map Tuple.second
    in
        if String.isEmpty input then
            -- no input display the first letters of all options.
            bank
                |> List.map (String.left 1)
                |> Set.fromList
                |> Set.toList
                |> List.sort
        else
            bank
                |> List.filter (String.startsWith input)
                |> List.sort


isWin : Maybe String -> String -> AnswerBank -> Bool
isWin answer guess answerBank =
    case answer of
        Nothing ->
            False

        Just answer ->
            case
                answerBank
                    |> List.filter (\( hiragana, romaji ) -> hiragana == answer)
                    |> List.head
                    |> Maybe.map Tuple.second
                    |> Maybe.map ((==) guess)
            of
                Nothing ->
                    False

                Just n ->
                    n


buildInitialAnswerSequence : AnswerBank -> Random.Seed -> ( List String, Random.Seed )
buildInitialAnswerSequence answerBank seed =
    let
        runner =
            answerBank
                |> List.map Tuple.first
                |> Random.List.shuffle
                |> Random.step
    in
        runner seed


complement : (a -> Bool) -> a -> Bool
complement predicate =
    (\a -> (not (predicate a)))


inputIsPotentialAnswer : String -> AnswerBank -> Bool
inputIsPotentialAnswer input answerBank =
    getMatches input answerBank
        |> List.filter ((==) input)
        |> List.isEmpty
