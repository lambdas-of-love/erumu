--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Model exposing (..)

import Keyboard.Extra exposing (Key(..))
import Time
import Random


type Msg
    = KeyDown Key
    | KeyUp Key
    | OnTime Time.Time


type alias Model =
    { content : String
    , guesses : Int
    , guessesOnCurrentQuestion : Int
    , currentHints : List String
    , nextSeed : Random.Seed
    , won : Int
    , playIncorrectAnimation : Bool
    , question : Maybe String
    , questions : List String
    }


model : Model
model =
    { content = ""
    , guessesOnCurrentQuestion = 0
    , nextSeed = Random.initialSeed 0
    , guesses = 0
    , won = 0
    , playIncorrectAnimation = False
    , currentHints = []
    , questions = []
    , question = Nothing
    }
