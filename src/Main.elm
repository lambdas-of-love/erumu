--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Main exposing (..)

import Html
import Task
import Keyboard.Extra
import Keyboard.Extra exposing (Key(..))
import Time
import Model exposing (..)
import View exposing (..)
import Update exposing (update)


getTime : Cmd Msg
getTime =
    Time.now
        |> Task.perform OnTime


init : ( Model, Cmd Msg )
init =
    ( model, getTime )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Keyboard.Extra.downs KeyUp
        , Keyboard.Extra.ups KeyDown
        ]


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , update = update
        , init = init
        , subscriptions = subscriptions
        }
