--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Update exposing (update)

import Random
import Keyboard.Extra exposing (toCode, Key(..))
import Time
import Char
import Util exposing (..)
import Model exposing (..)
import Answers exposing (..)


updateHandleOnTime : Model -> Time.Time -> ( Model, Cmd Msg )
updateHandleOnTime model time =
    let
        ( shuffled, nextSeed ) =
            buildInitialAnswerSequence hiraganaMonoGraphs (Random.initialSeed (floor (Time.inMilliseconds time)))

        questions =
            shuffled
    in
        ( { model
            | questions = List.drop 1 questions
            , question = List.head questions
            , nextSeed = nextSeed
          }
        , Cmd.none
        )


updateHandleBackspace : Model -> ( Model, Cmd Msg )
updateHandleBackspace model =
    ( { model | content = String.dropRight 1 model.content }, Cmd.none )


updateHandleRomaji : Model -> Char.KeyCode -> ( Model, Cmd Msg )
updateHandleRomaji model keyCode =
    let
        newContent =
            keyCode
                |> Char.fromCode
                |> String.fromChar
                |> String.toLower
                |> String.append model.content
    in
        ( { model
            | content =
                -- Only update if we found matches in the answer database.
                if getMatches newContent hiraganaMonoGraphs |> List.isEmpty then
                    model.content
                else
                    newContent
            , playIncorrectAnimation = False
          }
        , Cmd.none
        )


updateModelForWin : Model -> Model
updateModelForWin model =
    let
        newQuestions =
            List.drop 1 model.questions

        ( newNewQuestions, nextSeed ) =
            if List.isEmpty newQuestions then
                -- Build a new random sequence and generate a new seed.
                buildInitialAnswerSequence hiraganaMonoGraphs model.nextSeed
            else
                ( newQuestions, model.nextSeed )
    in
        { model
            | content = ""
            , guesses = model.guesses + 1
            , guessesOnCurrentQuestion = 0
            , questions = newNewQuestions
            , currentHints = []
            , nextSeed = nextSeed
            , currentHints = []
            , question = List.head model.questions
            , won = model.won + 1
        }



-- TODO if a person gets a question wrong add it back into the set to re-inforce.


updateModelForLoss : Model -> Model
updateModelForLoss model =
    let
        ( shuffled, nextSeed ) =
            calculateHints hiraganaMonoGraphs model.nextSeed model.question
    in
        { model
            | content = ""
            , playIncorrectAnimation = True
            , guesses = model.guesses + 1
            , nextSeed = nextSeed
            , currentHints =
                if model.guessesOnCurrentQuestion == 0 then
                    shuffled
                else
                    model.currentHints
            , guessesOnCurrentQuestion = model.guessesOnCurrentQuestion + 1
        }


updateHandleSubmit : Model -> ( Model, Cmd Msg )
updateHandleSubmit model =
    if inputIsPotentialAnswer model.content hiraganaMonoGraphs then
        -- the user isn't on a possible answer so ignore the keypress.
        ( model, Cmd.none )
    else if isWin model.question model.content hiraganaMonoGraphs then
        ( updateModelForWin model, Cmd.none )
    else
        ( updateModelForLoss model, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnTime time ->
            updateHandleOnTime model time

        KeyDown keyCode ->
            ( model, Cmd.none )

        KeyUp key ->
            let
                keyCode =
                    toCode key
            in
                if key == BackSpace then
                    updateHandleBackspace model
                else if keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 then
                    updateHandleRomaji model keyCode
                else if key == Enter then
                    updateHandleSubmit model
                else
                    -- ignore everything else
                    ( model, Cmd.none )
