--  Erumu - a tool for learning Japanese
--
--  Copyright (C) 2018 Lambdas of Love LLC
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


module Answers exposing (..)


type alias AnswerBank =
    List ( String, String )


hiraganaMonoGraphs : List ( String, String )
hiraganaMonoGraphs =
    [ ( "あ", "a" )
    , ( "い", "i" )
    , ( "う", "u" )
    , ( "え", "e" )
    , ( "お", "o" )
    , ( "か", "ka" )
    , ( "き", "ki" )
    , ( "く", "ku" )
    , ( "け", "ke" )
    , ( "こ", "ko" )
    , ( "さ", "sa" )
    , ( "し", "shi" )
    , ( "す", "su" )
    , ( "せ", "se" )
    , ( "そ", "so" )
    , ( "た", "ta" )
    , ( "ち", "chi" )
    , ( "つ", "tsu" )
    , ( "て", "te" )
    , ( "と", "to" )
    , ( "な", "na" )
    , ( "に", "ni" )
    , ( "ぬ", "nu" )
    , ( "ね", "ne" )
    , ( "の", "no" )
    , ( "は", "ha" )
    , ( "ひ", "hi" )
    , ( "ふ", "fu" )
    , ( "へ", "he" )
    , ( "ほ", "ho" )
    , ( "ま", "ma" )
    , ( "み", "mi" )
    , ( "む", "mu" )
    , ( "め", "me" )
    , ( "も", "mo" )
    , ( "や", "ya" )
    , ( "ゆ", "yu" )
    , ( "よ", "yo" )
    , ( "ら", "ra" )
    , ( "り", "ri" )
    , ( "る", "ru" )
    , ( "れ", "re" )
    , ( "ろ", "ro" )
    , ( "わ", "wa" )
    , ( "を", "wo" )
    , ( "ん", "n" )
    ]
