# erumu 🌳

A tool for learning hiragana/katakana/kanji.

# Installation

This project is bootstrapped with [Create Elm App](https://github.com/halfzebra/create-elm-app).

	npm install -g elm create-elm-app

Now you can do:

	elm-app start

Things will automatically reload as you change them.

## Prod build

	elm-app build

## REPL

	elm-app repl
